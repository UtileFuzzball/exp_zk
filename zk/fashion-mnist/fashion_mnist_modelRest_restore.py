import numpy as np
import tensorflow as tf
import  pandas as pd
tf.reset_default_graph()
import  time
from keras.utils import to_categorical
mnist = tf.keras.datasets.fashion_mnist.load_data()
train_data,test_data = np.reshape(mnist[0][0],(len(mnist[0][0]),784)),np.reshape(mnist[1][0],(len(mnist[1][0]),784))
train_label,test_label = to_categorical(mnist[0][1]),to_categorical(mnist[1][1])

batch_size = 100
learning_rate = 0.003
weight_decay = 0.0003
momentum_rate = 0.9
iterations = 100
batch_size = 500
total_epoch = 1100
learning_rate_decay = 0.97
model_save_path= "/content/drive/My Drive/my_driver/FashionMnistModelREST/"
slim = tf.contrib.slim

def batch_norm(input):
    return tf.contrib.layers.batch_norm(input, decay=0.9, center=True, scale=True, epsilon=1e-3,
                                        is_training=False, updates_collections=None)
def res_identity(input_tensor, conv_depth, kernel_shape, layer_name):
    with tf.variable_scope(layer_name):
        relu = tf.nn.relu(batch_norm(slim.conv2d(input_tensor, conv_depth, kernel_shape)))
        outputs = tf.nn.relu(batch_norm(slim.conv2d(relu, conv_depth, kernel_shape) + input_tensor))
    return outputs


def res_change(input_tensor, conv_depth, kernel_shape, layer_name):
    input_depth = input_tensor.shape[3]
    with tf.variable_scope(layer_name):
        relu = tf.nn.relu(batch_norm(slim.conv2d(input_tensor, conv_depth, kernel_shape, stride=2)))
        input_tensor_reshape = slim.conv2d(input_tensor, conv_depth, [1, 1], stride=2)
        outputs = tf.nn.relu(batch_norm(slim.conv2d(relu, conv_depth, kernel_shape) + input_tensor_reshape))
    return outputs

def learning_rate_schedule(epoch_num):
    if epoch_num < 81:
        return 0.1
    elif epoch_num < 121:
        return 0.01
    else:
        return 0.001

def inference(inputs,train_flag):
	x = tf.reshape(inputs, [-1, 28, 28, 1])
	conv_1 = tf.nn.relu(slim.conv2d(x, 32, [3, 3])) #28*28*32
	pool_1 = slim.max_pool2d(conv_1, [2, 2]) #14*14*32
	block_1 = res_identity(pool_1, 32, [3, 3], 'layer_2')
	block_2 = res_identity(block_1, 32, [3, 3], 'layer_3')
	block_3 = res_identity(block_2, 32, [3, 3], 'layer_4') #14*14*32
	block_4 = res_change(block_3, 64, [3, 3], 'layer_5')
	block_5 = res_identity(block_4, 64, [3, 3], 'layer_6')
	block_6 = res_identity(block_5, 64, [3, 3], 'layer_7')
	net_flatten = slim.flatten(block_6, scope='flatten')
	fc_1 = slim.fully_connected(slim.dropout(net_flatten, 0.8), 200, activation_fn=tf.nn.relu, scope='fc_1')
	output = slim.fully_connected(slim.dropout(fc_1, 0.8), 10, activation_fn=None, scope='output_layer')
	return output


def train():
    x = tf.placeholder(tf.float32, [None, 784])
    y = tf.placeholder(tf.float32, [None, 10])
    y_outputs = inference(x,True)
    global_step = tf.Variable(0, trainable=False)

    entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=y_outputs, labels=tf.argmax(y, 1))
    loss = tf.reduce_mean(entropy)
    l2 = tf.add_n([tf.nn.l2_loss(var) for var in tf.trainable_variables()])
    rate = tf.train.exponential_decay(learning_rate, global_step, 200, learning_rate_decay)
    train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss, global_step=global_step)
    train_step = tf.train.MomentumOptimizer(learning_rate, momentum_rate, use_nesterov=True).minimize(loss + l2 * weight_decay)
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_outputs, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    flag = tf.cast(correct_prediction, tf.float32)
    saver = tf.train.Saver(max_to_keep=10)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        ckpt = tf.train.get_checkpoint_state(model_save_path)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            print("model load over")
        graph = tf.get_default_graph()
        fc1 = graph.get_operation_by_name('fc_1/Relu').outputs[0]
        fc1_output,fc2_output,corr_flag_list,acc_ = sess.run([fc1,y_outputs,flag,accuracy],feed_dict={x: test_data, y: test_label})
        print(acc_)
        np.save("/content/drive/My Drive/my_driver/data/fashion_mnist/modelRest/corr_flag.npy", corr_flag_list)
        id_list = [i for i in range(200 * 10000)]
        picture_id = [j + 1 for j in range(10000) for i in range(200)]
        position = [_ + 1 for _ in range(200)] * 10000
        value = np.reshape(fc1_output, (10000 * 200))
        data_ = {"id": id_list, "picture_id": picture_id, "position": position, "value": value}
        pd.DataFrame(data_).to_csv("/content/drive/My Drive/my_driver/data/fashion_mnist/modelRest/fc1_result.csv")
        id_list = [i for i in range(10 * 10000)]
        picture_id = [j + 1 for j in range(10000) for i in range(10)]
        position = [_ + 1 for _ in range(10)] * 10000
        value = np.reshape(fc2_output, (10000 * 10))
        data_ = {"id": id_list, "picture_id": picture_id, "position": position, "value": value}
        pd.DataFrame(data_).to_csv("/content/drive/My Drive/my_driver/data/fashion_mnist/modelRest/fc2_result.csv")



def main():
    train()

if __name__ == '__main__':
    main()