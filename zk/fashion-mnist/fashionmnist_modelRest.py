import numpy as np
import tensorflow as tf

tf.reset_default_graph()
import  time
from keras.utils import to_categorical
mnist = tf.keras.datasets.fashion_mnist.load_data()
train_data,test_data = np.reshape(mnist[0][0],(len(mnist[0][0]),784)),np.reshape(mnist[1][0],(len(mnist[1][0]),784))
train_label,test_label = to_categorical(mnist[0][1]),to_categorical(mnist[1][1])

batch_size = 100
learning_rate = 0.003
weight_decay = 0.0003
momentum_rate = 0.9
iterations = 100
batch_size = 500
total_epoch = 1100
learning_rate_decay = 0.97
model_save_path= "/content/drive/My Drive/my_driver/FashionMnistModelREST/"
slim = tf.contrib.slim

def batch_norm(input):
    return tf.contrib.layers.batch_norm(input, decay=0.9, center=True, scale=True, epsilon=1e-3,
                                        is_training=False, updates_collections=None)
def res_identity(input_tensor, conv_depth, kernel_shape, layer_name):
    with tf.variable_scope(layer_name):
        relu = tf.nn.relu(batch_norm(slim.conv2d(input_tensor, conv_depth, kernel_shape)))
        outputs = tf.nn.relu(batch_norm(slim.conv2d(relu, conv_depth, kernel_shape) + input_tensor))
    return outputs


def res_change(input_tensor, conv_depth, kernel_shape, layer_name):
    input_depth = input_tensor.shape[3]
    with tf.variable_scope(layer_name):
        relu = tf.nn.relu(batch_norm(slim.conv2d(input_tensor, conv_depth, kernel_shape, stride=2)))
        input_tensor_reshape = slim.conv2d(input_tensor, conv_depth, [1, 1], stride=2)
        outputs = tf.nn.relu(batch_norm(slim.conv2d(relu, conv_depth, kernel_shape) + input_tensor_reshape))
    return outputs

def learning_rate_schedule(epoch_num):
    if epoch_num < 81:
        return 0.1
    elif epoch_num < 121:
        return 0.01
    else:
        return 0.001

def inference(inputs,train_flag):
	x = tf.reshape(inputs, [-1, 28, 28, 1])
	conv_1 = tf.nn.relu(slim.conv2d(x, 32, [3, 3])) #28*28*32
	pool_1 = slim.max_pool2d(conv_1, [2, 2]) #14*14*32
	block_1 = res_identity(pool_1, 32, [3, 3], 'layer_2')
	block_2 = res_identity(block_1, 32, [3, 3], 'layer_3')
	block_3 = res_identity(block_2, 32, [3, 3], 'layer_4') #14*14*32
	block_4 = res_change(block_3, 64, [3, 3], 'layer_5')
	block_5 = res_identity(block_4, 64, [3, 3], 'layer_6')
	block_6 = res_identity(block_5, 64, [3, 3], 'layer_7')
	net_flatten = slim.flatten(block_6, scope='flatten')
	fc_1 = slim.fully_connected(slim.dropout(net_flatten, 0.8), 200, activation_fn=tf.nn.relu, scope='fc_1')
	output = slim.fully_connected(slim.dropout(fc_1, 0.8), 10, activation_fn=None, scope='output_layer')
	return output


def train():
    x = tf.placeholder(tf.float32, [None, 784])
    y = tf.placeholder(tf.float32, [None, 10])
    y_outputs = inference(x,True)
    global_step = tf.Variable(0, trainable=False)

    entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=y_outputs, labels=tf.argmax(y, 1))
    loss = tf.reduce_mean(entropy)
    l2 = tf.add_n([tf.nn.l2_loss(var) for var in tf.trainable_variables()])
    rate = tf.train.exponential_decay(learning_rate, global_step, 200, learning_rate_decay)
    train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss, global_step=global_step)
    train_step = tf.train.MomentumOptimizer(learning_rate, momentum_rate, use_nesterov=True).minimize(loss + l2 * weight_decay)
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_outputs, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    saver = tf.train.Saver(max_to_keep=10)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        ckpt = tf.train.get_checkpoint_state("")
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            print("model load over")
        for ep in range(1, total_epoch + 1):
            lr = learning_rate_schedule(ep)
            pre_index = 0
            train_acc = 0.0
            train_loss = 0.0
            start_time = time.time()

            print("\n epoch %d/%d:" % (ep, total_epoch))

            for it in range(1, iterations + 1):
                batch_x = train_data[pre_index:pre_index + batch_size]
                batch_y = train_label[pre_index:pre_index + batch_size]
                _, batch_loss = sess.run([train_step, loss],
                                         feed_dict={x: batch_x, y: batch_y})
                batch_acc = accuracy.eval(feed_dict={x: batch_x, y: batch_y})

                train_loss += batch_loss
                train_acc += batch_acc
                pre_index += batch_size

                if ep % 20 == 0:
                    train_loss /= iterations
                    train_acc /= iterations

                    loss_, acc_ = sess.run([loss, accuracy],
                                           feed_dict={x: batch_x, y: batch_y})
                    print("iteration: %d/%d, cost_time: %ds, train_loss: %.4f, "
                          "train_acc: %.4f"
                          % (it, iterations, int(time.time() - start_time), train_loss, train_acc))
                    break
                else:
                    print("iteration: %d/%d, train_loss: %.4f, train_acc: %.4f"
                          % (it, iterations, train_loss / it, train_acc / it), end='\r')
            if ep % 50 == 0:
                save_path = saver.save(sess, model_save_path, global_step=ep)
                print("Model saved in file: %s" % save_path)


def main():
    train()

if __name__ == '__main__':
    main()