from tensorflow.examples.tutorials.mnist import input_data
import os
import numpy as np
import tensorflow as tf
# import connect_mysql as mysql_connect
MODEL_SAVE_PATH = "/content/drive/My Drive/my_driver/mnistmodelfullconnect/"
MODEL_NAME = "MNISTAMODEL.ckpt"
#将所有的图片重新设置尺寸为32*32
w = 28
h = 28
c = 1
#mnist数据集中训练数据和测试数据保存地址
mnist = input_data.read_data_sets("/content/drive/My Drive/my_driver/mnist_train_data/",one_hot=True) #读取mnist 数据集
train_data,train_label = mnist.train.images,mnist.train.labels
train_data = np.reshape(train_data,(train_data.shape[0],w*h))
test_data,test_label = mnist.test.images,mnist.test.labels
test_data = np.reshape(test_data,(test_data.shape[0],w*h))
# print(test_label)
#打乱训练数据及测试数据
train_image_num = len(train_data)
train_image_index = np.arange(train_image_num)
np.random.shuffle(train_image_index)
train_data = train_data[train_image_index]
train_label = train_label[train_image_index]

test_image_num = len(test_data)
test_image_index = np.arange(test_image_num)
np.random.shuffle(test_image_index)
test_data = test_data[test_image_index]
test_label = test_label[test_image_index]

#搭建CNN
x = tf.placeholder(tf.float32,[None,w*h],name='x')
y_ = tf.placeholder(tf.float32,[None,10],name='y_')

def inference(input_tensor,train,regularizer):
    with tf.variable_scope('layer1-fc1'):
        fc1_weights = tf.get_variable('weight',[w*h,500],initializer=tf.truncated_normal_initializer(stddev=0.1))
        if regularizer != None:
            tf.add_to_collection('losses',regularizer(fc1_weights))
        fc1_biases = tf.get_variable('bias',[500],initializer=tf.constant_initializer(0.1))
        fc1 = tf.nn.relu(tf.matmul(x,fc1_weights) + fc1_biases)
        tf.add_to_collection("fc1",fc1)
        if train:
            fc1 = tf.nn.dropout(fc1,0.5)

    with tf.variable_scope('layer2-fc2'):
        fc2_weights = tf.get_variable('weight',[500,300],initializer=tf.truncated_normal_initializer(stddev=0.1))
        if regularizer != None:
            tf.add_to_collection('losses',regularizer(fc2_weights))
        fc2_biases = tf.get_variable('bias',[300],initializer=tf.constant_initializer(0.1))
        fc2 = tf.nn.relu(tf.matmul(fc1,fc2_weights) + fc2_biases)
        tf.add_to_collection("fc2",fc2)
        if train:
            fc2 = tf.nn.dropout(fc2,0.5)

    with tf.variable_scope('layer3-fc3'):
        fc3_weights = tf.get_variable('weight',[300,150],initializer=tf.truncated_normal_initializer(stddev=0.1))
        if regularizer != None:
            tf.add_to_collection('losses',regularizer(fc3_weights))
        fc3_biases = tf.get_variable('bias',[150],initializer=tf.constant_initializer(0.1))
        fc3 = tf.nn.relu(tf.matmul(fc2,fc3_weights) + fc3_biases)
        tf.add_to_collection("fc3",fc3)
        if train:
            fc3 = tf.nn.dropout(fc3,0.5)

    #第六层：全连接层，120->84的全连接
    #尺寸变化：比如一组训练样本为64，那么尺寸变化为64×120->64×84
    #第二层 设置为120
    with tf.variable_scope('layer4-fc4'):
        fc4_weights = tf.get_variable('weight',[150,80],initializer=tf.truncated_normal_initializer(stddev=0.1))
        if regularizer != None:
            tf.add_to_collection('losses',regularizer(fc4_weights))
        fc4_biases = tf.get_variable('bias',[80],initializer=tf.truncated_normal_initializer(stddev=0.1))
        fc4 = tf.nn.relu(tf.matmul(fc3,fc4_weights) + fc4_biases)
        tf.add_to_collection("fc4", fc4)
        if train:
            fc4 = tf.nn.dropout(fc4,0.5)

    #第七层：全连接层（近似表示），84->10的全连接
    #尺寸变化：比如一组训练样本为64，那么尺寸变化为64×84->64×10。最后，64×10的矩阵经过softmax之后就得出了64张图片分类于每种数字的概率，
    #即得到最后的分类结果。
    with tf.variable_scope('layer5-fc5'):
        fc5_weights = tf.get_variable('weight',[80,10],initializer=tf.truncated_normal_initializer(stddev=0.1))
        if regularizer != None:
            tf.add_to_collection('losses',regularizer(fc5_weights))
        fc5_biases = tf.get_variable('bias',[10],initializer=tf.truncated_normal_initializer(stddev=0.1))
        logit = tf.matmul(fc4,fc5_weights) + fc5_biases
        tf.add_to_collection("fc5", logit)
    return logit




def main():
        # 正则化，交叉熵，平均交叉熵，损失函数，最小化损失函数，预测和实际equal比较，tf.equal函数会得到True或False，
        # accuracy首先将tf.equal比较得到的布尔值转为float型，即True转为1.，False转为0，最后求平均值，即一组样本的正确率。
        # 比如：一组5个样本，tf.equal比较为[True False True False False],转化为float型为[1. 0 1. 0 0],准确率为2./5=40%。
    regularizer = tf.contrib.layers.l2_regularizer(0.001)
    y = inference(x,True,regularizer)
    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=y,labels=tf.argmax(y_,1))
    cross_entropy_mean = tf.reduce_mean(cross_entropy)
    loss = cross_entropy_mean + tf.add_n(tf.get_collection('losses'))
    train_op = tf.train.AdamOptimizer(0.001).minimize(loss)
    correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))

    #每次获取batch_size个样本进行训练或测试
    saver = tf.train.Saver()
    #创建Session会话
    with tf.Session() as sess:
        #初始化所有变量(权值，偏置等)
        sess.run(tf.global_variables_initializer())

        def get_batch(data, label, batch_size):
            for start_index in range(0, len(data) - batch_size + 1, batch_size):
                slice_index = slice(start_index, start_index + batch_size)
                yield data[slice_index], label[slice_index]

        #将所有样本训练10次，每次训练中以64个为一组训练完所有样本。
        #train_num可以设置大一些。
        train_num = 2001
        batch_size = 256
        for i in range(train_num):
            train_loss,train_acc,batch_num = 0, 0, 0
            for train_data_batch,train_label_batch in get_batch(train_data,train_label,batch_size):
                _,err,acc= sess.run([train_op,loss,accuracy],feed_dict={x:train_data_batch,y_:train_label_batch})
                train_loss+=err;train_acc+=acc;batch_num+=1
            print("epoch:",i)
            print("train loss:",train_loss/batch_num)
            print("train acc:",train_acc/batch_num)
            # 25轮 保存一次模型
            if i%100==0:
                saver.save(sess, os.path.join(MODEL_SAVE_PATH, MODEL_NAME), global_step=i)

if __name__=="__main__":

    main()
