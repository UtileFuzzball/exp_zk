import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random
from functools import reduce
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from sklearn.cluster import KMeans
import operator

# file1_path="/home/wangsiqi/w77/PycharmProjects/exp_zk/zk/mnist/data/mnist/modelA/"
file1_path="/home/wangsiqi/w77/PycharmProjects/exp_zk/zk/mnist/新建文件夹/"
file1_name = "fc2_result.csv" #训练数据
flag_file = "corr_flag.npy" #测试数据flag

full_cnt = 84
fre_num = 10

mnist = input_data.read_data_sets("./MNIST_data/",one_hot=True)
# mnist = tf.keras.datasets.cifar10.load_data() #读取mnist 数据集
test_label = np.argmax(mnist.test.labels,axis=1)

label = test_label
print(label)

reshape_label = [_ for _ in test_label for j in range(full_cnt)]

flag = np.load(file1_path+flag_file)
print(flag.sum())
fc1_data = pd.read_csv(file1_path+file1_name)
fc1_data["label"]=reshape_label
fc1_data = fc1_data[fc1_data["value"]>0]

def count_APFD(sort_order):
	flag = np.load(flag_file)
	sort_flag = flag[sort_order]
	wrong_item_id = []
	for i in range(10000):
		if sort_flag[i]==0:
			wrong_item_id.append(i)
	result = 1 +1/2/len(sort_order) -np.array(wrong_item_id).sum()/len(sort_order)/len(wrong_item_id)
	return result

def act_neu_cov(fc1_data):
	fc1_data = pd.read_csv(file1_path+file1_name)
	fc1_data = fc1_data[fc1_data["value"]>0]
	fc1_cov = fc1_data.groupby("picture_id").size()
	cov_fc1 = []
	for i in range(1,10001,1):
		if i in fc1_cov.index:
			cov_fc1.append(fc1_cov.ix[i])
		else:
			cov_fc1.append(0)
	cov = np.array(cov_fc1)
	id_list	= [i for i in range(10000)]
	data = {"id":id_list,"cov":cov}	
	sort = list(pd.DataFrame(data).sort_values("cov",ascending=False)["id"])
	return sort

def critical_nenron(fc1_data):
	k_list = [[] for i in range(10)]
	cnt_list = []
	for i in range(10):
		for j in range(1,full_cnt+1):
			k_list[i].append(fc1_data[(fc1_data["label"]==i)&(fc1_data["position"]==j)].count()[0])
		num = [[_] for _ in k_list[i]]
		k_model=KMeans(n_clusters=2).fit(num)
		label_ = np.array(k_model.labels_)
		current_data = np.array(k_list[i])
		if(current_data[label_==0].mean()>current_data[label_==1].mean()):
			cnt_list.append(len(label_[label_==0]))
		else:
			cnt_list.append(len(label_[label_==1]))
	k_list = cnt_list #
	print(k_list)
	right=[[] for i in range(10)]
	for	i in range(10000):
		if flag[i]==1:
			right[label[i]].append(i)

	rightlist = [[] for i in range(10)]
	#获取所有正确数据激活位置
	for i in range(10):
		for j in right[i]:
			rightlist[i].append(fc1_data[fc1_data["picture_id"]==j+1]["position"].tolist())
	percent_list = []

	right_mode = [[] for _ in range(10)]
	error_mode = [[] for _ in range(10)]
	for i in range(10):
		temp = np.array(reduce(operator.add,rightlist[i]))
		temp_list=[len(temp[temp==j]) for j in range(512)]
		temp_list = pd.Series(temp_list)
		right_mode[i] = list(temp_list.sort_values(ascending=False).index[0:k_list[i]])
		error_mode[i] = list(temp_list.sort_values(ascending=False).index[k_list[i]:])
	return right_mode,error_mode
def critical_cov(right_mode,error_mode):
	allpositionnum=[]
	for i in range(10000):
		temp = fc1_data[fc1_data["picture_id"]==(i+1)]["position"].tolist()
		allpositionnum.append(len(set(temp)&set(right_mode[label[i]]))/(1.0+len(set(temp)&set(error_mode[label[i]]))))   
	id_list	= [i for i in range(10000)]
	data = {"id":id_list,"cov":allpositionnum}	
	sort = list(pd.DataFrame(data).sort_values("cov",ascending=True)["id"])
	return sort
def frequent_cov(right_mode,error_mode):
	# right_mode,error_mode = critical_nenron(fc1_data)
	allpositionnum=[]
	for i in range(10000):
		temp = fc1_data[fc1_data["picture_id"]==(i+1)]["position"].tolist()
		allpositionnum.append(len(set(temp)&set(right_mode[label[i]])))
	id_list	= [i for i in range(10000)]
	data = {"id":id_list,"cov":allpositionnum}	
	sort = list(pd.DataFrame(data).sort_values("cov",ascending=True)["id"])
	return sort

def random_plot():
	flag = np.load(file1_path+flag_file)
	acc_wrong_cnt = []
	cnt = 0
	flag_ran = flag[:]
	for j in range(10000):
		cnt = cnt+1-flag_ran[j]
		acc_wrong_cnt.append(cnt) 
	return acc_wrong_cnt
def ideal_plot():
	flag = np.load(file1_path+flag_file)
	acc_wrong_cnt = []
	cnt = 0
	flag.sort()
	for j in range(10000):
		cnt = cnt+1-flag[j]
		acc_wrong_cnt.append(cnt) 
	return acc_wrong_cnt
def acc_plot(sort):
	flag = np.load(file1_path+flag_file)
	sort_flag = flag[sort]
	acc_wrong_cnt = []
	cnt = 0
	for j in range(10000):
		cnt = cnt+1-sort_flag[j]
		acc_wrong_cnt.append(cnt) 
	return acc_wrong_cnt

print("获取频繁激活子集与非频繁激活子集")
right_mode,error_mode = critical_nenron(fc1_data)
# print("频繁激活子集为")
# for i in range(10):
# 	print("类别",i,"的频繁激活子集为：",right_mode[i])
# 	print("类别",i,"的非频繁激活子集为：",error_mode[i])
# print("非频繁激活子集为")
# for i in range(10):
# 	print("类别",i,"的频繁激活子集为：",error_mode[i])
print()
print()
print("开始排序")
print()
print()
# print("--------------神经元覆盖率--------")
# sort_cov = act_neu_cov(fc1_data)
# print("APFD:",count_APFD(sort_cov))
# print("--------------频繁神经元覆盖率--------")
# sort_fre = frequent_cov(right_mode,error_mode)
# print("APFD:",count_APFD(sort_fre))
# print("--------------关键神经元覆盖率--------")
sort_crit = critical_cov(right_mode,error_mode)
print("排完序后的测试集序列为：")
print(sort_crit)
print()
print()
print("此序列的APFD值：",count_APFD(sort_crit))
print()
print()
# print("APFD:",count_APFD(sort_crit))
# l1 = plt.plot(acc_plot(sort_cov))
# l2 = plt.plot(acc_plot(sort_fre))
l3 = plt.plot(acc_plot(sort_crit))
l4 = plt.plot(ideal_plot(),linestyle="--")
l5 = plt.plot(random_plot())
plt.legend([l3,l4,l5],labels=["frequently activated neurons","ideal","random"],loc = 'lower right')
# plt.legend([l1,l2,l3,l4,l5],labels=["neuron coverage","frequently neuron coverage","critical neuron coverage","ideal","random"],loc = 'lower right')
plt.xlabel("Number of test cases")
plt.ylabel("Cumulative number of errors")
# plt.savefig("/content/drive/My Drive/my_driver/pic/new/cifar_D.png")
# plt.savefig("/content/drive/My Drive/my_driver/pic/new/cifar_D.pdf")
print("累计错误曲线为")
plt.show()