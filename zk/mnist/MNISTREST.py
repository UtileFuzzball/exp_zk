import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from keras.utils import to_categorical
mnist = tf.keras.datasets.mnist.load_data()
train_data,test_data = np.reshape(mnist[0][0],(len(mnist[0][0]),784)),np.reshape(mnist[1][0],(len(mnist[1][0]),784))
train_label,test_label = to_categorical(mnist[0][1]),to_categorical(mnist[1][1])

mnist = input_data.read_data_sets("/content/drive/My Drive/my_driver/mnist_train_data", one_hot=True)
# mnist = input_data.read_data_sets("./MNIST_data", one_hot=True)

batch_size = 100
learning_rate = 0.003
learning_rate_decay = 0.97
model_save_path= "/content/drive/My Drive/my_driver/MnistModelREST/"
slim = tf.contrib.slim

def res_identity(input_tensor, conv_depth, kernel_shape, layer_name):
    with tf.variable_scope(layer_name):
        relu = tf.nn.relu(slim.conv2d(input_tensor, conv_depth, kernel_shape))
        outputs = tf.nn.relu(slim.conv2d(relu, conv_depth, kernel_shape) + input_tensor)
    return outputs


def res_change(input_tensor, conv_depth, kernel_shape, layer_name):
    input_depth = input_tensor.shape[3]
    with tf.variable_scope(layer_name):
        relu = tf.nn.relu(slim.conv2d(input_tensor, conv_depth, kernel_shape, stride=2))
        input_tensor_reshape = slim.conv2d(input_tensor, conv_depth, [1, 1], stride=2)
        outputs = tf.nn.relu(slim.conv2d(relu, conv_depth, kernel_shape) + input_tensor_reshape)
    return outputs

def inference(inputs,train_flag):
	x = tf.reshape(inputs, [-1, 28, 28, 1])
	conv_1 = tf.nn.relu(slim.conv2d(x, 32, [3, 3])) #28*28*32
	pool_1 = slim.max_pool2d(conv_1, [2, 2]) #14*14*32
	block_1 = res_identity(pool_1, 32, [3, 3], 'layer_2')
	block_2 = res_identity(block_1, 32, [3, 3], 'layer_3')
	block_3 = res_identity(block_2, 32, [3, 3], 'layer_4') #14*14*32
	block_4 = res_change(block_3, 64, [3, 3], 'layer_5')
	block_5 = res_identity(block_4, 64, [3, 3], 'layer_6')
	block_6 = res_identity(block_5, 64, [3, 3], 'layer_7')
	net_flatten = slim.flatten(block_6, scope='flatten')
	fc_1 = slim.fully_connected(slim.dropout(net_flatten, 0.8), 200, activation_fn=tf.nn.relu, scope='fc_1')
	output = slim.fully_connected(slim.dropout(fc_1, 0.8), 10, activation_fn=None, scope='output_layer')
	return output


def train():
    x = tf.placeholder(tf.float32, [None, 784])
    y = tf.placeholder(tf.float32, [None, 10])
    y_outputs = inference(x,True)
    global_step = tf.Variable(0, trainable=False)

    entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=y_outputs, labels=tf.argmax(y, 1))
    loss = tf.reduce_mean(entropy)
    rate = tf.train.exponential_decay(learning_rate, global_step, 200, learning_rate_decay)
    train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss, global_step=global_step)
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_outputs, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    saver = tf.train.Saver(max_to_keep=10)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        # tensor_name_list = [tensor.name for tensor in tf.get_default_graph().as_graph_def().node]
        # print(tensor_name_list)
        # graph = tf.get_default_graph()
        # fc1 = graph.get_operation_by_name('fc_1/Relu').outputs[0]

        for i in range(2001):
            x_b, y_b = mnist.train.next_batch(batch_size)
            train_op_, loss_, step ,acc= sess.run([train_op, loss, global_step,accuracy], feed_dict={x: x_b, y: y_b})
            print("training step {0}, loss {1},acc{2}".format(step, loss_, acc))
            # print(fc1_output.shape)
            if i % 100 == 0:
                acc = sess.run(accuracy, feed_dict={x: test_data, y: test_label})
                print(acc)
                saver.save(sess, model_save_path + 'my_model', global_step=global_step)


def main():
    train()


if __name__ == '__main__':
    main()
    # print(mnist.test.images.shape)
    # print(mnist.test.labels.shape)



